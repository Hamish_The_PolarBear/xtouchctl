# xtouchctl

#### A tool to allow users of the Behringer Xtouch to remap and assign controls.


This is for the XTouch control surface made by Behringer. This supports the Xtouch in
Mackie mode over MIDI or USB-MIDI. This system *may* work with other control surfaces
that use the Mackie protocol.

The XTouch protocol is a bit quirky so some functions
can only be accessed via MIDI (not USB-MIDI).

These scripts are being written on a GNU/Linux based system, some functions may  
not work as expected on other operating systems.

(Planned) Functions
---

* Special `SYSEX` message constantly sent/checked to keep MIDI link alive
* Reads and filters valid inputs from the device
* Valid inputs are checked against a mapping file
    * Any matching inputs then trigger the appropriate action in a target application
    * Any feedback actions are triggered (LED states etc) according to the mapping file
* Snapshots (see below) can be made of the board state to allow channel/bank switching or "modes"


Snapshots: The message processor maintains a data structure with the current state of the
XTouch LEDs, displays, faders, encoders and button LEDs. This allows paging of the
controls and indicators.

